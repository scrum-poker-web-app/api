<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserCredentials;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Создание временного пользователя
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function storeTemp(Request $request): JsonResponse
    {
        //todo: включить валидацию после того как тестировщик определит проблему
//        $this->validate($request, [
//            'name' => ['required', 'max:255', 'regex:/^[a-zа-я\d\s_\-$,.]*$/i']
//        ]);

        $user = new User($request->all(['name']));
        $user->save();

        return response()->json($user);
    }

    /**
     * Создание зарегистрированного пользователя (имеет логин и пароль)
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => ['required', 'max:255', 'regex:/^[a-zа-я\d\s_\-$,.]*$/i'],
            'email' => ['required', 'email', 'unique:user_credentials'],
            'password' => ['required'],
        ]);

        $user = DB::transaction(function () use ($request) {
            $user = new User($request->all(['name']));
            $user->save();
            $userCredentials = new UserCredentials();
            $userCredentials->fill([
                'email' => $request->get('email'),
                'password_hash' => Hash::make($request->get('password')),
            ]);

            $user->credentials()->save($userCredentials);
            return $user;
        });

        return response()->json($user);
    }
}
