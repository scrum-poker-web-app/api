<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuestsController extends Controller
{
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $id = DB::table('guests')->insertGetId(['name' => $request->get('name')]);
        $user = DB::selectOne('select * from guests where id = ?', [$id]);
        return response()->json($user);
    }
}
